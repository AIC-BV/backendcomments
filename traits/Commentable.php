<?php
namespace StudioBosco\BackendComments\Traits;
trait Commentable
{
    /**
     * @var string Name of backend comments relation.
     */
    public $backendCommentsRelation = 'comments';

    /*
     * Constructor
     */
    public static function bootCommentable()
    {
        static::extend(function ($model) {
            /*
             * Define relationships
             */
            $model->morphMany[$model->backendCommentsRelation] = [
                'StudioBosco\BackendComments\Models\Comment',
                'name' => 'commentable',
                'delete' => true,
            ];
        });
    }

    public function comment($body, $authorId = null)
    {
        $backendCommentsRelation = $this->backendCommentsRelation;

        $this->{$backendCommentsRelation}()->create([
            'body' => $body,
            'author_id' => $authorId,
        ]);
    }
}
