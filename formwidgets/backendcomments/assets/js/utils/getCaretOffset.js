function getCaretOffset(textarea) {
    const cursorPos = textarea.selectionStart;
    const textBeforeCursor = textarea.value.substring(0, cursorPos);
    const textAfterCursor = textarea.value.substring(cursorPos);
    const pre = document.createTextNode(textBeforeCursor);
    const post = document.createTextNode(textAfterCursor);
    const caretEl = document.createElement('span');
    caretEl.innerHTML = '&nbsp;';
    const mirroredEl = document.createElement('div');
    const textareaStyle = window.getComputedStyle(textarea);
    document.body.appendChild(mirroredEl);
    mirroredEl.style.position = 'fixed';
    mirroredEl.style.boxSizing = 'border-box';
    mirroredEl.style.top = 0;
    mirroredEl.style.left = 0;
    mirroredEl.style.font = textareaStyle.font;
    mirroredEl.style.lineHeight = textareaStyle.lineHeight;
    mirroredEl.style.fontSize = textareaStyle.fontSize;
    mirroredEl.style.fontWeight = textareaStyle.fontWeight;
    mirroredEl.style.letterSpacing = textareaStyle.letterSpacing;
    mirroredEl.style.padding = textareaStyle.padding;
    mirroredEl.style.whiteSpace = textareaStyle.whiteSpace;
    mirroredEl.style.overflow = textareaStyle.overflow;
    mirroredEl.innerHTML = '';
    mirroredEl.append(pre, caretEl, post);
    mirroredEl.style.width = textarea.clientWidth + 'px';
    mirroredEl.style.height = textarea.clientHeight + 'px';
    mirroredEl.scrollTo(textarea.scrollLeft, textarea.scrollTop);

    const caretElBBox = caretEl.getBoundingClientRect();
    mirroredEl.remove();

    return {
        x: caretElBBox.left,
        y: caretElBBox.top,
        height: parseFloat(textareaStyle.lineHeight),
    }
}

export default getCaretOffset;
