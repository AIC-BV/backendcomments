/**
 * Determines offset of one element to another in x,y coordinates.
 * @param {Element} el
 * @param {Element} otherEl
 * @returns Object {x,y}
 */
function getOffsetTo(el, otherEl) {
  const elBBox = el.getBoundingClientRect();
  const otherElBBox = otherEl.getBoundingClientRect();

  return {
    x: elBBox.left - otherElBBox.left,
    y: elBBox.top - otherElBBox.top,
  }
}

export default getOffsetTo;
