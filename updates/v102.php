<?php namespace StudioBosco\BackendComments\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V102 extends Migration
{
    public function up()
    {
        Schema::table('studiobosco_backendcomments_comments', function (Blueprint $table) {
            $table->index('author_id');
            $table->index('commentable_id');
            $table->index('commentable_type');
        });
    }

    public function down()
    {
        Schema::table('studiobosco_backendcomments_comments', function (Blueprint $table) {
            $table->dropIndex([
                'author_id',
                'commentable_id',
                'commentable_type',
            ]);
        });
    }
}
