<?php namespace StudioBosco\BackendComments\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V101 extends Migration
{
    public function up()
    {
        Schema::create('studiobosco_backendcomments_comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('body')->nullable();
            $table->bigInteger('author_id')->unsigned();
            $table->bigInteger('commentable_id')->unsigned();
            $table->string('commentable_type', 1024);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('studiobosco_backendcomments_comments');
    }
}
