<?php namespace StudioBosco\BackendComments\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V103 extends Migration
{
    public function up()
    {
        Schema::table('studiobosco_backendcomments_comments', function (Blueprint $table) {
            $table->string('source_url', 1024)->nullable();
        });
    }

    public function down()
    {
        Schema::table('studiobosco_backendcomments_comments', function (Blueprint $table) {
            $table->dropColumn([
                'source_url',
            ]);
        });
    }
}
