<?php
namespace StudioBosco\BackendComments;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'studiobosco.backendcomments::lang.plugin.name',
            'description' => 'studiobosco.backendcomments::lang.plugin.description',
            'author'      => 'Ondrej Brinkel <ondrej@studiobosco.de>',
            'icon'        => 'icon-comment-o',
        ];
    }

    public function registerNavigation()
    {
        return [
            'backendcomments' => [
                'label' => 'studiobosco.backendcomments::lang.comments',
                'url' => Backend::url('studiobosco/backendcomments/comments'),
                'icon' => 'icon-comment-o',
                'permissions' => ['studiobosco.backendcomments::manage_comments'],
                'order' => 500,
                /*'counter' => $count,
                'counterLabel' => Lang::get(
                    'studiobosco.backendcomments::lang.comments_counter_label.' . ($count === 1 ? 'singular' : 'plural'),
                    ['count' => $count]
                ),*/
            ],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'StudioBosco\BackendComments\FormWidgets\BackendComments' => 'backendcomments',
        ];
    }

    public function registerPermissions()
    {
        return [
            'studiobosco.backendcomments::manage_comments' => [
                'label' => 'studiobosco.backendcomments::lang.permissions.manage_comments',
                'tab' => 'studiobosco.backendcomments::lang.plugin.name',
            ],
            'studiobosco.backendcomments::delete_comments' => [
                'label' => 'studiobosco.backendcomments::lang.permissions.delete_comments',
                'tab' => 'studiobosco.backendcomments::lang.plugin.name',
            ],
        ];
    }
}
