# WinterCMS Backend Comments Plugin

Provides commenting in the backend.

## Usage

This plugin provides the `StudioBosco\BackendComments\Traits\Commentable` Trait and a `comments` field widget.

To make a model commentable

1. just add the trait:

```php
class MyModel
{
    use StudioBosco\BackendComments\Traits\Commentable;
    ...
```

You can set the name of the comments relation in your model if you want anything different then `comments`:

```php

    /**
     * @var string Name of backend comments relation.
     */
    public $backendCommentsRelation = 'myComments';
```


2. Add the comments field to it's model form:

```yaml
fields:
    _comments:
        type: backendcomments
        dateFormat: d-m-Y H:i:s # use your custom dateformat if required (it also accepts translation strings)
        context: # use update and preview context only as comments do not work deffered
            - update
            - preview
```

The "comments" field must not have the same name as the relation. By default the relation is called "comments".
So use "_comments" for the field name or anything else.

## Mentions

If you have the [`StudioBosco.BackendNotifications`](https://packagist.org/packages/studiobosco/wn-backendnotifications-plugin) plugin installed backend comments allow mentions by writing `@username` or `@first_name` or `@last_name` or `@first_and_last_name`.

This will trigger an event for every mention before it is turned into a notification.

You can listen to the following event:

`studiobosco.backendcomments.mention`

**Event arguments**

- `StudiBosco\BackendComments\Classes\CommentMention` - `$mention` The mention before it triggers a notification. You can mutate this object to e.g. adjusts the notification URL.

An example on how to change the URL of the notification:

```php

Event::listen('studiobosco.backendcomments.mention', function ($mention) {
    $commentable = $mention->comment->commentable;

    if ($commentable) {
        if ($commentable instanceof MyModel) {
            $mention->url = Backend::url('myplugin/mymodel/update/' . $commentable->id);
        }
    }
});
```
