<?php namespace StudioBosco\BackendComments\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Comments Back-end Controller
 */
class Comments extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('StudioBosco.BackendComments', 'backendcomments', 'comments');
    }

    public function listExtendQuery($query)
    {
        if ($this->user && $this->user->hasAccess('studiobosco.backendcomments::manage_comments')) {
            return $query;
        }

        $query->listBackend();
        return $query;
    }
}
