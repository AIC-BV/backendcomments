<?php namespace StudioBosco\BackendComments\Behaviors;

use Flash;
use Backend\Classes\ControllerBehavior;

class CommentController extends ControllerBehavior
{
    /**
     * @var bool Has the behavior been initialized.
     */
    protected $initialized = false;

    /**
     * @var bool Disables the ability to add, update, delete or create relations.
     */
    public $readOnly = false;

    /**
     * @var int Source record id.
     */
    protected $recordId;

    /**
     * @var array Source record ids.
     */
    protected $recordIds = [];

    /**
     * @var \Backend\Classes\Controller|FormController Reference to the back end controller.
     */
    protected $controller;

    /**
     * The controller action is responsible for supplying the parent model
     * so it's action must be fired. Additionally, each AJAX request must
     * supply the relation's field name (_relation_field).
     */
    protected function beforeAjax()
    {
        if ($this->initialized) {
            return;
        }

        $this->prepareVars();
        $this->initialized = true;
    }

    /**
     * Prepares the view data.
     * @return void
     */
    public function prepareVars()
    {
        $this->recordIds = post('record_ids', []);

        if ($this->recordIds && is_string($this->recordIds)) {
            $this->recordIds = explode(',', $this->recordIds);
        }

        $this->vars['recordIds'] = $this->recordIds;
        $this->vars['relation'] = post('relation', null);
    }

    public function onCommentMultipleButton()
    {
        $this->beforeAjax();

        // The form should not share its session key with the parent
        $this->vars['newSessionKey'] = str_random(40);

        return $this->makePartial('comment_multiple_form');
    }

    public function onCommentMultipleForm()
    {
        $this->beforeAjax();

        if (!$this->recordIds || !count($this->recordIds)) {
            return;
        }

        /*
         * Establish the list definition
         */
        $definition = post('definition', null);
        $listConfig = $this->controller->listGetConfig($definition);

        if (!$listConfig) {
            throw new ApplicationException(Lang::get('backend::lang.list.missing_parent_definition', compact('definition')));
        }

        // retrieve comment data
        $data = post('Comment');
        $body = array_get($data, 'body');

        if (!$body) {
            Flash::error(trans('studiobosco.backendcomments::lang.comment_multiple_error'));
            return;
        }

        /*
         * Create the model
         */
        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->controller->listExtendModel($model, $definition);

        /*
         * Create the query
         */
        $query = $model->newQuery();
        $this->controller->listExtendQueryBefore($query, $definition);

        $query->whereIn($model->getKeyName(), $this->recordIds);
        $this->controller->listExtendQuery($query, $definition);

        $records = $query->get();

        foreach($records as $record) {
            $record->comment($body);
        }

        Flash::success(trans('studiobosco.backendcomments::lang.comment_multiple_success'));

        return;
    }

    public function onCommentMultipleRelatedButton($recordId)
    {
        $this->beforeAjax();

        // The form should not share its session key with the parent
        $this->vars['newSessionKey'] = str_random(40);
        $this->vars['recordId'] = $recordId;

        return $this->makePartial('comment_multiple_related_form');
    }

    public function onCommentMultipleRelatedForm($recordId)
    {
        $this->beforeAjax();

        if (!$this->recordIds || !count($this->recordIds)) {
            return;
        }

        $relation = post('_relation_field', null);

        // retrieve comment data
        $data = post('Comment');
        $body = array_get($data, 'body');

        if (!$body || !$relation) {
            Flash::error(trans('studiobosco.backendcomments::lang.comment_multiple_error'));
            return;
        }

        /*
         * Create the model
         */
        $model = $this->controller->formFindModelObject($recordId);

        /*
         * Create the query
         */
        $query = $model->{$relation}();
        $relatedKey = $query->getQualifiedRelatedKeyName();
        $records = $query->whereIn($relatedKey, $this->recordIds)->get();

        foreach($records as $record) {
            $record->comment($body);
        }

        Flash::success(trans('studiobosco.backendcomments::lang.comment_multiple_success'));

        return method_exists($this->controller, 'afterCommentMultipleRelatedFormSubmit')
            ? $this->controller->afterCommentMultipleRelatedFormSubmit($recordId)
            : null;
    }
}
